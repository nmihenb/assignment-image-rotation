#include <stdio.h>
#ifndef ASSIGNMENT_IMAGE_ROTATION_RUN_H
#define ASSIGNMENT_IMAGE_ROTATION_RUN_H
int transformation_image(const char* path_to_source_image, const char* path_to_result_image);
#endif //ASSIGNMENT_IMAGE_ROTATION_RUN_H
