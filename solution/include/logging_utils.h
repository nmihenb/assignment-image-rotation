//
// Created by nikita on 1/9/23.
//

#ifndef IMAGE_TRANSFORMER_LOGGING_UTILS_H
#define IMAGE_TRANSFORMER_LOGGING_UTILS_H
enum errors{
    NO_ERROR = 0,
    FILE_OPEN_ERROR = 1,
    HEADERS_READ_ERROR = 2,
    IMAGE_DATA_ERROR = 3,
    FILE_CLOSE_ERROR = 4,
    TRANSFORMATION_ERROR = 5,
    WRITE_FILE_ERROR = 6,

};
enum statuses {
    NO_STATUS = 0,
    FILE_READ_STATUS = 1,
    HEADERS_READ_STATUS = 2,
    IMAGE_DATA_STATUS = 3,
    FILE_CLOSE_STATUS = 4,
    TRANSFORMATION_STATUS = 5,
    WRITE_FILE_STATUS = 6,
    EXECUTION_STATUS = 7

};
struct execution_information{
    enum errors log_error;
    enum statuses log_status;
    const char *str_args;
};

void show_log_info_term(struct execution_information* log_information);
#endif //IMAGE_TRANSFORMER_LOGGING_UTILS_H
