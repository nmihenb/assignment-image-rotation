#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_UTILS_H

#include "bmp_headers.h"
#include <bits/stdint-uintn.h>
#include <bits/types/FILE.h>




struct pixel { uint8_t b, g, r; };
struct image get_image_data(const struct bmp_headers* headers, FILE *file, struct execution_information* log);
void write_file(struct bmp_headers headers, FILE* file, struct image new_image, struct execution_information* log);
struct image rotate(struct image source, struct execution_information* log);
void destruct_image(struct image* image);
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_UTILS_H
