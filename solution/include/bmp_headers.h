//
// Created by nikita on 1/4/23.
//

#ifndef IMAGE_TRANSFORMER_BMP_HEADERS_H
#define IMAGE_TRANSFORMER_BMP_HEADERS_H
#pragma pack(push, 1)

#include "logging_utils.h"
#include <bits/stdint-uintn.h>
#include <stdio.h>

struct bmp_headers
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct bmp_headers get_headers(FILE *file, struct execution_information* log);
struct bmp_headers build_new_headers(struct image *source_image);
#endif //IMAGE_TRANSFORMER_BMP_HEADERS_H
