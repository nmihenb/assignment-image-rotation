#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
#include "logging_utils.h"
#include <stdio.h>
#include <stdlib.h>

enum file_status{
    READ_B = 0,
    WRITE_B = 1,
};

FILE * open_file(const char *filename, enum file_status f_status, struct execution_information* log);
void close_file(FILE *file, const char *filename, struct execution_information* log);
#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
