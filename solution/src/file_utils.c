#include "../include/file_utils.h"
#include "stdio.h"

static char* const file_mode[] = {
        "rb",
        "wb",
};
FILE* open_file(const char *filename, enum file_status f_status, struct execution_information* log)
{
    FILE *fp = fopen(filename, file_mode[f_status]);
    if (fp == NULL)
    {
        log->log_error = FILE_OPEN_ERROR;
    }else{
        log->log_status = FILE_READ_STATUS;
    }
    log->str_args = filename;
    return fp;
}
void close_file(FILE *file, const char *filename, struct execution_information* log){
    if (fclose(file) == 0){
        log->log_status = FILE_CLOSE_STATUS;
    }else{
        log->log_error = FILE_CLOSE_ERROR;
    }
    log->str_args = filename;
}





