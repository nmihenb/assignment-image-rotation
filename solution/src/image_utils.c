#include "../include/image_utils.h"
#include <malloc.h>


 uint64_t static get_padding(const struct bmp_headers* headers){
    return (4 - headers->biWidth * sizeof(struct pixel) % 4) % 4;
}

static struct image construct_image(uint64_t width, uint64_t height){
    struct image image = {
            .data = malloc(height * width * sizeof(struct pixel)),
                    .height = height,
                    .width = width,
    };
    return image;
}

void destruct_image(struct image* image){
    free(image->data);
}
static void get_pixels_data(const struct bmp_headers* headers, struct image* image,
        FILE* file, struct execution_information* log) {
    FILE* local_file = file;
    long paddings = (long)get_padding(headers);
    for (size_t row = 0; row < headers->biHeight; row++) {
        if (fread(image->data + row * headers->biWidth,sizeof (struct pixel),headers->biWidth,
                local_file) == 0 || fseek(local_file,paddings,SEEK_CUR) != 0) {
            log->log_error = IMAGE_DATA_ERROR;
            free(file);
            break;
        }
        if (row == headers->biHeight - 1){
            log->log_status = IMAGE_DATA_STATUS;
        }
    }
}
struct image get_image_data(const struct bmp_headers* headers, FILE *file, struct execution_information* log){
    struct image source_image = construct_image(headers->biWidth, headers->biHeight);
    get_pixels_data(headers, &source_image, file, log);
    return source_image;
}
static void set_image_data_in_file(const struct bmp_headers* new_headers, struct image new_image, FILE* file,
        struct execution_information* log){
    char colors[3] = {0,0,0};
    uint64_t padding = get_padding(new_headers);
    for (size_t row = 0; row < new_headers->biHeight; row++) {
        if (fwrite(new_image.data + row * new_headers->biWidth, sizeof(struct pixel),
                new_headers->biWidth, file) != new_image.width || fwrite(colors, 1,
                                                                      padding, file) != padding) {
            log->log_error = WRITE_FILE_ERROR;
            break;
        }
        if (row == new_headers->biHeight - 1){
            log->log_status = WRITE_FILE_STATUS;
        }
    }
}

void write_file(struct bmp_headers headers, FILE* file, struct image new_image, struct execution_information* log){
    if (fwrite(&headers, sizeof (struct bmp_headers), 1, file)){
        log->log_status = WRITE_FILE_STATUS;
    }else{
        log->log_error = WRITE_FILE_ERROR;
        return;
    }
    set_image_data_in_file(&headers, new_image, file, log);
}
struct image rotate(struct image source, struct execution_information* log){
    struct image new_image = construct_image(source.height, source.width);
    for (size_t h = 0; h < source.height; h++) {
        for (size_t w = 0; w < source.width; w++) {
            new_image.data[w * source.height + (source.height - 1 - h)] = source.data[h * source.width + w];
        }
    }
    if (new_image.data == NULL){
        log->log_error = TRANSFORMATION_ERROR;
    }else{
        log->log_status = TRANSFORMATION_STATUS;
    }
    return new_image;
}



