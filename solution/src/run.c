#include "bmp_headers.h"
#include "file_utils.h"
#include "image_utils.h"
#include "logging_utils.h"

int transformation_image(const char* path_to_source_image, const char* path_to_result_image){
    struct execution_information log_execution = {
            .log_error = NO_ERROR,
            .log_status = NO_STATUS,
            .str_args = " "
    };
    FILE *source_file = open_file(path_to_source_image, READ_B, &log_execution);
    show_log_info_term(&log_execution);
    const struct bmp_headers source_headers = get_headers(source_file, &log_execution);
    show_log_info_term(&log_execution);
    struct image image = get_image_data(&source_headers, source_file, &log_execution);
    show_log_info_term(&log_execution);
    close_file(source_file, path_to_source_image, &log_execution);
    show_log_info_term(&log_execution);
    FILE *result_file = open_file(path_to_result_image, WRITE_B, &log_execution);
    show_log_info_term(&log_execution);
    struct image new_image = rotate(image, &log_execution);
    show_log_info_term(&log_execution);
    destruct_image(&image);
    struct bmp_headers new_headers = build_new_headers(&new_image);
    write_file(new_headers, result_file, new_image, &log_execution);
    show_log_info_term(&log_execution);
    destruct_image(&new_image);
    close_file(result_file, path_to_result_image, &log_execution);
    show_log_info_term(&log_execution);
    log_execution.log_status = EXECUTION_STATUS;
    log_execution.str_args = path_to_result_image;
    show_log_info_term(&log_execution);
    return 0;
}
