//
// Created by nikita on 1/4/23.
//
#include "../include/image_utils.h"
#include <malloc.h>
struct bmp_headers get_headers(FILE *file, struct execution_information* log){
    struct bmp_headers headers;
    if (fread(&headers, sizeof(headers), 1, file)){
        log->log_status = HEADERS_READ_STATUS;
    }else{
        log->log_error = HEADERS_READ_ERROR;
    }
    return headers;
}
struct bmp_headers build_new_headers(struct image *source_image) {
    struct bmp_headers headers;
    headers.bfileSize = source_image->height * source_image->width * sizeof(struct pixel) + sizeof (struct bmp_headers);
    headers.biSize = 40;
    headers.bfType = 19778;
    headers.biPlanes = 1;
    headers.biBitCount = 24;
    headers.biWidth = source_image->width;
    headers.biHeight = source_image->height;
    headers.bOffBits = 54;
    headers.biYPelsPerMeter = 2834;
    headers.biClrImportant = 0;
    headers.bfReserved = 0;
    headers.biCompression = 0;
    headers.biSizeImage = source_image->height * source_image->width * sizeof(struct pixel);
    headers.biXPelsPerMeter = 2834;
    headers.biClrUsed = 0;
    return headers;
}
