#include "run.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    if (argc == 3) {
        return transformation_image(argv[1], argv[2]);
    }else{
        fprintf(stderr, "[ERROR] Invalid arguments");
        return 1;
    }
}

