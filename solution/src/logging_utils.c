//
// Created by nikita on 1/9/23.
//
#include "../include/logging_utils.h"
#include <stdio.h>

static char* const error_messages[] = {
        "No error",
        "[ERROR] Could not open file",
        "[ERROR] Attempt to read the headlines failed",
        "[ERROR]! Unreadable row or failed set cursor",
        "[ERROR] File not closed",
        "[ERROR] Transformation image failed",
        "[ERROR] Failed set image data!"
};
static char* const status_messages[] = {
        "No status",
        "[INFO] File opened",
        "[INFO] Headers read",
        "[INFO] Source image read",
        "[INFO] File closed",
        "[INFO] Transformation image success",
        "[INFO] Set data in image success!",
        "[INFO] The picture has been successfully rotated 90 degrees and is along the following path:"

};
static char* get_error_message(struct execution_information* log_information){
    char* result_data = error_messages[log_information->log_error];
    log_information->log_error = NO_ERROR;
    return result_data;
}
static char* get_status_message(struct execution_information* log_information){
    char* result_data = status_messages[log_information->log_status];
    log_information->log_status = NO_STATUS;
    return result_data;
}
const static char* get_str_args(struct execution_information* log_information) {
    const char* result_data = log_information->str_args;
    log_information->str_args = " ";
    return result_data;
}

void show_log_info_term(struct execution_information* log_information){
    if (log_information->log_error){
        fprintf(stderr, "%s %s\n", get_error_message(log_information), get_str_args(log_information));
    }
    if (log_information->log_status){
        fprintf(stdout, "%s %s\n", get_status_message(log_information), get_str_args(log_information));
    }
}


