file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    include/*.h
)

add_executable(image-transformer ${sources} include/bmp_headers.h src/bmp_headers.c include/logging_utils.h src/logging_utils.c)
target_include_directories(image-transformer PRIVATE src include)
